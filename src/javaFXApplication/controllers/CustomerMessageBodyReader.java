package javaFXApplication.controllers;

import javaFXApplication.entities.Customer;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.ws.rs.Consumes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Provider
@Consumes({"application/json"})
public class CustomerMessageBodyReader implements MessageBodyReader<List<Customer>> {

    @Override
    public boolean isReadable(Class<?> type, Type type1, Annotation[] antns,
                              MediaType mt) {
        return true;
    }

    @Override
    public List<Customer> readFrom(Class<List<Customer>> type, Type type1, Annotation[] antns, MediaType mt, MultivaluedMap<String, String> mm, InputStream in) throws IOException,  WebApplicationException {
        if (mt.getType().equals("application") && mt.getSubtype().equals("json")) {
            Customer customer = new Customer();
            List<Customer> customers = new ArrayList();
            JsonParser parser = Json.createParser(in);
            while (parser.hasNext()) {
                JsonParser.Event event = parser.next();
                switch (event) {
                    case START_OBJECT:
                        customer = new Customer();
                        break;
                    case END_OBJECT:
                        customers.add(customer);
                        break;
                    case KEY_NAME:
                        String key = parser.getString();
                        parser.next();
                        switch (key) {
                            case "name":
                                customer.setName(parser.getString());
                                break;
                            case "username":
                                customer.setUsername(parser.getString());
                                break;
                            case "email":
                                customer.setEmail(parser.getString());
                                break;
                            case "balance":
                                customer.setBalance(parser.getInt());
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
            return customers;
        }
        throw new UnsupportedOperationException("Not supported MediaType: " + mt);
    }
}
