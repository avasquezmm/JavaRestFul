package javaFXApplication.model.util;

import javaFXApplication.entities.Customer;
import javaFXApplication.entities.CustomerTblModel;

/**
 * Created by anvargear on 8/05/17.
 */
public class Convert {

    /**
     * The properties of each User retrieved from REST service
     * are set to an UserTableModel object and it is added to the table
     * @param customer User getted from REST service
     * @return UserTableModel with properties values of User
     */
    public static CustomerTblModel toCustomerTblModel(Customer customer) {
        CustomerTblModel customerTblModel = new CustomerTblModel();
        customerTblModel.setId(customer.getId());
        customerTblModel.setName(customer.getName());
        customerTblModel.setUsername(customer.getUsername());
        customerTblModel.setType(customer.getType());
        customerTblModel.setEmail(customer.getEmail());
        return customerTblModel;
    }
}
