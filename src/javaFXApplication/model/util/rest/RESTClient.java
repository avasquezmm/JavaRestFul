package javaFXApplication.model.util.rest;

import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import javaFXApplication.entities.Customer;

public class RESTClient {

    //private static final String WS_URI = "http://localhost:8080/customersapp/api/customers";
    private static final String WS_URI = "http://publiclicks.app/api/v10/user";


    public static Customer updateCustomer(Customer customer) throws RuntimeException {
        Client client = null;
        try {
            client = ClientBuilder.newClient();
            WebTarget target = client.target(getBaseUri());
            Customer updated = target.path("update").request()
                    .put(Entity.entity(customer, MediaType.APPLICATION_XML), Customer.class);
            return updated;
        } catch(RuntimeException e) {
            throw e;
        } finally { if(client != null) client.close(); }
    }

    /* *************************************
     * 			 UTIL METHODS
     ***************************************/
    // Check the status of RESTful WebService
    public static boolean checkWS() {
        Boolean stateOfWS = false;
        try {
            URL siteURL = new URL(getBaseUri().toString());
            HttpURLConnection connection = (HttpURLConnection) siteURL.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            int code = connection.getResponseCode();
            if(code == 200) stateOfWS = true;
        } catch(Exception e) {
            // do nothing
        }
        return stateOfWS;
    }
    private static URI getBaseUri() {
        return UriBuilder.fromUri(WS_URI).build();
    }
}
